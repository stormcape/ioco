package com.iocotask.invoices.controllers;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iocotask.invoices.entities.Invoice;
import com.iocotask.invoices.entities.LineItem;
import com.iocotask.invoices.repositories.InvoiceRepository;
import com.iocotask.invoices.repositories.LineItemRepository;

import lombok.extern.slf4j.Slf4j;




@RestController
@Transactional
@Slf4j
public class InvoiceController {
	
	@Value("${spring.datasource.url}")
	private String h2datasource;

	@Value("${spring.datasource.username}")
	private String h2datasourceUsername;
	
	@Value("${spring.datasource.password}")
	private String h2datasourcePassword;
	
	Logger logger = LoggerFactory.getLogger(InvoiceController.class);
	private final InvoiceRepository invoiceRepository;
	private final LineItemRepository lineItemRepository;
	
	InvoiceController ( InvoiceRepository invoiceRepository,
						LineItemRepository lineItemRepository) {
		this.invoiceRepository = invoiceRepository;
		this.lineItemRepository = lineItemRepository;
	}
	
	
	@RequestMapping("/")
	public String index() {
		return menuTop("Home");
	}
	
	@GetMapping("/invoices")
	String showlist() {
		logger.info("listing");
		
		List<Invoice> invoices = invoiceRepository.findAll();
		
		String output =  menuTop("Listing all invoices"); 
		output += "<table >";
		output += "<tr><th colspan=6 class='inverse'>INVOICES LIST</th></tr>";

		output += "<tr>";
		output += "<th>Client</th>";
		output += "<th>Vat rate</th>" ;
		output += "<th>Invoice date</th>"; 
		output += "<th>Total</th>"; 
		output += "<th>Line detail</th>";
		output += "<th>Delete</th>";
		
		output += "</tr>";

		for (Invoice invoice : invoices) {
			output += "<tr>";
			output += "<td>" + invoice.client + "</td>";
			output += "<td style=\"text-align:center\">" + invoice.vatRate + "</td>" ;
			output += "<td>" + displayFormattedDate(invoice.invoiceDate) + "</td>"; 
			output += "<td style=\"text-align:center\">" + invoice.getTotal() + "</td>"; 
			
			output += "<td style='text-align:center'> <a href='http://localhost:8080/invoice/"+invoice.id+"'>View detail</a></td>";
			output += "<td style='text-align:center'> <a class='redlink' href='http://localhost:8080/deleteinvoice/"+invoice.id+"' onclick=\"return confirm('Are you sure?')\">Delete</a></td>";
			output += "</tr>";
		}
		output += "</table>";
		return output;
		
	}

	@GetMapping("/invoice/{invoiceId}")
	String showSingle(@PathVariable Long invoiceId) {
		logger.info("single");
		
		String output = invoiceAndDetail(invoiceId);
		return output;
		
	}
	
	@GetMapping("/deleteinvoice/{invoiceId}")
	String deleteInvoice(@PathVariable Long invoiceId) {
		logger.info("deleteinvoice "+invoiceId);
		
		String output = deleteInvoiceService(invoiceId);
		return menuTop(output) ;
		
	}
	
	public String deleteInvoiceService(Long invoiceId) {
		
		Optional<Invoice> inv  = invoiceRepository.findById(invoiceId);
		
		Invoice invoice = inv.get();
		
		if (invoice == null) {
			return "Invoice #"+invoiceId+" not found.";
		}

		List<LineItem> lineItems = lineItemRepository.findByInvoiceId(invoiceId);

		String lineItemsDeleted = ". Line items: ";
		if (lineItems.size() > 0) {
			for (LineItem lineItem : lineItems) {
				lineItemsDeleted += " "+lineItem.id;
				lineItemRepository.delete(lineItem);
			}
		}
		
		invoiceRepository.delete(invoice);
		
		return "Deleted inv #"+invoiceId+lineItemsDeleted;
	}
	

	@GetMapping("/deleteline/{lineItemId}")
	String deletelineItem(@PathVariable Long lineItemId) {
		logger.info("deleteinvoiceLine "+lineItemId);
		
		String output = deleteLineService(lineItemId);
		return menuTop(output) ;
		
	}

	public String deleteLineService(Long lineItemId) {
		
		Optional<LineItem> lineItem = lineItemRepository.findById(lineItemId);
		
		if (lineItem == null) {
			return "Line Item #"+lineItem+" not found.";
		}
		
		lineItemRepository.delete(lineItem.get());
		return "Deleted line item #"+lineItemId;
		
	}
	
	public String invoiceAndDetail(long invoiceId) {
		
		Optional<Invoice> inv  = invoiceRepository.findById(invoiceId);
		
		Invoice invoice = inv.get();
		
		if (invoice == null) {
			return "Invoice #"+invoiceId+" not found.";
		}
		
		BigDecimal invTotal = invoice.getTotal();
		String refresh = " <a href='http://localhost:8080/invoice/"+invoice.id+"'> Refresh</a>";
		
		String output =  menuTop("Invoice and detail")+"<br><h2>INVOICE #"+invoiceId+" </h2><Br>";
		output += "<form action='http://localhost:8080/updateinvoiceget' method='GET'>";
		output += "<table>";
		output += "<tr><th class=\"inverse\" colspan=4>Invoice header</th></tr>";
		output += "<tr>";
		
		output += "<th>Client</th>";
		output += "<th>Vat rate</th>" ;
		output += "<th>Invoice date</th>"; 
		output += "<th>Update<br> invoice <br> header</th>";
		output += "</tr>";

		output += "<tr>";
		output += " <input name='invoice_id' value='" + invoiceId + "' type=\"hidden\">";
		output += "<td>                             <input name='client' value='" + invoice.client + "'></td>";
		output += "<td style='text-align:center'> <input name='vat_rate' value='" + invoice.vatRate + "' type=\"number\"></td>" ;
		output += "<td>                             <input name='invoice_date' value='" + displayFormattedDate(invoice.invoiceDate) + "' style=\"width:220px\" type=\"date\"></td>"; 
		output += "<td>  <input type=\"submit\" value=\"UPDATE\"</td>";
		output += "</tr>";
	
		
		output +="<tr><th class=\"inverse\" colspan=3>Total "+invTotal+"</th>";
		output +="<th class=\"inverse\" >"+refresh+"</th>";
		output +="</tr>";
		
		output += "</table>";
		output += "</form>";

		
		String detailForm = "<form action='http://localhost:8080/addlineitem' method='GET'" 
		+ ">"
		+ "<table>"
		+ "<tr><th class=\"inverse\" colspan=2>Add Line Item to Invoice #"+invoiceId+"</th></tr>"
		+ "<tr><td>Invoice Id</td> <td><input name='invoice_id' value="+invoiceId+" readonly></td>"
		+ "<tr><td>Quantity </td>  <td><input name='quantity' type=\"number\"></td>"
		+ "<tr><td>Description</td><td><input name='description'></td>"
		+ "<tr><td>UnitPrice</td>  <td><input name='unit_price' type=\"number\"></td>"
		+ "<tr><td>GO</td>         <td><input type='submit' name='submit' value='Submit'></td>"
		+ "</table>"
		+ "</form>";
		
		List<LineItem> lineItems = lineItemRepository.findByInvoiceId(invoiceId);
		
		String lineItemsLines = "<table>";
		lineItemsLines += "<tr><th  class=\"inverse\" colspan=5>Line items</th></tr>";
		lineItemsLines += "<tr>";
		
//		lineItemsLines += "<th>Invoice</th>";
		lineItemsLines += "<th>Description</th>";
		lineItemsLines += "<th>Quantity</th>" ;
		lineItemsLines += "<th>Unit price</th>"; 
		lineItemsLines += "<th>Update</th>"; 
		lineItemsLines += "<th>Delete</th>"; 
		lineItemsLines += "</tr>";

		if (lineItems.size() > 0) {
			for (LineItem lineItem : lineItems) {
				lineItemsLines +="<form action='http://localhost:8080/updatelineitem' method='GET'>";
				lineItemsLines += "<tr>";
				lineItemsLines += "<input type=\"hidden\" name='id' value='" + lineItem.id + "' >";
				lineItemsLines += "<td><input name='description' value='" + lineItem.description + "'></td>" ;
				lineItemsLines += "<td style='text-align:center'><input name='quantity' type='number' value='" + lineItem.quantity + "'></td>"; 
				lineItemsLines += "<td style='text-align:center'><input name='unit_price' type='number' value='" + lineItem.unitPrice + "'></td>";
				lineItemsLines += "<td style='text-align:center'><input type=\"submit\" style=\"width:120px\" name='submit' value='UPDATE'></td>";
				lineItemsLines += "<td style='text-align:center'>  <a class='redlink' href='http://localhost:8080/deleteline/"+invoice.id+"'  onclick=\"return confirm('Are you sure?')\">Delete</a></td>";
				lineItemsLines += "</tr>";
				lineItemsLines +="</form>";
			}
		}
		else {
			lineItemsLines += "<tr><td  colspan=5>No line items</td></tr>";
		}
		
		
		output += lineItemsLines;
		output += detailForm;
		
		
		return output;
		
	}

	@GetMapping(path = "/addinvoiceget")
	public String newInvoiceGet(
			@RequestParam (name = "client") String client,
			@RequestParam (name = "vat_rate") String vatRate,
			@RequestParam (name = "invoice_date") String invoiceDateString
									) 
	{
		logger.info("addinvoice get "+ client + ": "+ vatRate + ": "+  invoiceDateString);
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		
		Date invoiceDate = null;
		try {
			invoiceDate = formatter.parse(invoiceDateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		Invoice newInvoice = new Invoice(client, Long.valueOf(vatRate), invoiceDate);
		
		invoiceRepository.save(newInvoice);
		
		return invoiceAndDetail(newInvoice.id);
	}
	
	@GetMapping(path = "/updateinvoiceget")
	public String updateInvoiceGet(
			@RequestParam (name = "client") String client,
			@RequestParam (name = "vat_rate") String vatRate,
			@RequestParam (name = "invoice_date") String invoiceDateString,
			@RequestParam (name = "invoice_id") long invoiceId
									) {
		logger.info("updateinvoiceget "+ client + ": "+ vatRate + ": "+  invoiceDateString + " : "+invoiceId);
		
		if (!isNumeric(vatRate)) {
			return showError(vatRate + " is not a number");
		}
		
		Optional<Invoice> invoice = invoiceRepository.findById(invoiceId);
		
		Invoice getInvoice = invoice.get();
		
		if (getInvoice != null) {

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			
			Date invoiceDate = null;
			try {
				invoiceDate = formatter.parse(invoiceDateString);
				getInvoice.setInvoiceDate(invoiceDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			getInvoice.setVatRate(Long.valueOf(vatRate));
			getInvoice.setClient(client);
					
			invoiceRepository.save(getInvoice);
		}
		
		 return invoiceAndDetail(invoiceId);
	}
	
	
	@GetMapping(path = "/addlineitem")
	public String addlineitem(
			@RequestParam (name = "invoice_id") String invoiceId,
			@RequestParam (name = "quantity") String quantity,
			@RequestParam (name = "description") String description,
			@RequestParam (name = "unit_price") String unitPrice
									) 
	{
		logger.info("add lineitem "+ invoiceId + ": "+ quantity + ": "+  description + ": "+  unitPrice);
		
		if (!isNumeric(quantity)) {
			return showError(quantity + " is not a number");
		}
		
		if (!isNumeric(unitPrice)) {
			return showError(unitPrice + " is not a number");
		}
		
		
		LineItem newLineItem = new LineItem(
				Long.valueOf(quantity), 
				description,
				new BigDecimal(unitPrice),
				Long.valueOf(invoiceId)
				);
		
	   lineItemRepository.save(newLineItem);
	   return invoiceAndDetail(Long.valueOf(invoiceId));
	}
	

	@GetMapping(path = "/updatelineitem")
	public String updateLineItemGet(
			@RequestParam (name = "id") long id,
			@RequestParam (name = "description") String description,
			@RequestParam (name = "quantity") long quantity,
			@RequestParam (name = "unit_price") BigDecimal unitPrice
									) 
	{
		logger.info("updatelineitem "+ id + ": "+ description + ": "+  quantity + " : "+unitPrice);
		
		Optional<LineItem> lineItem = lineItemRepository.findById(id);
		
		LineItem getLineItem = lineItem.get();
		
		if (getLineItem != null) {

			getLineItem.setDescription(description);
			getLineItem.setQuantity(quantity);
			getLineItem.setUnitPrice(unitPrice);
					
			lineItemRepository.save(getLineItem);
			 return invoiceAndDetail(getLineItem.invoiceId);
		}
		
		 return menuTop("Not found: line item #"+id);
		
	}
	
	
	@GetMapping("/newinvoice/{client}/{vatrate}/{invoice_date}")
	public String newinvoice(@PathVariable String client,
			@PathVariable String vatrate,
			@PathVariable Date invoice_date
			) {
		logger.info("New "+client+ " " +vatrate);
		Invoice invoice = new Invoice(client,Long.valueOf(vatrate),invoice_date );
	   	invoiceRepository.save(invoice);
		return "New "+client+ " "+vatrate;

	}
	
	
	@GetMapping("/inv/add")
	String add() {
		logger.info("adding");
		String output =  menuTop("")+"<br><h2>ADD INVOICE</h2><Br>";
		output += "<form action='http://localhost:8080/addinvoiceget' method='GET'>";
		output += "<table >";
		output += "";
		output += "<tr><th>Client</th>      <td><input type='text' name='client'></td></tr>";
		output += "<tr><th>Vat rate</th>    <td><input type='number' name='vat_rate'></td></tr>" ;
		output += "<tr><th>Invoice date</th><td><input type='date' name='invoice_date'></td></tr>"; 
		output += "<tr><th>GO</th>          <td><input type='submit' name='submit' value='Submit'></td></tr>"; 
		output += "";
		output += "</form>"; 

		return output ;
		
	}


	
	String menuTop (String message) {
		return "<style>"
				+ "body {font-family:Arial}"
				+ "table th,table td{border:1px solid gray} "
				+ "table {border-collapse:collapse;margin-bottom:20px;width:800px} "
				+ "th.inverse, th.inverse a{color:#ffffff;background:blue} "
				+ "td input {width:130px}"
				+ ".redlink{color:red}"
				+ "</style>"
				+ "<script>"
				+ "function check() {"
				+ ""
				+ "alert(this.name)}"
				+ "</script>"
				+ "<h1>Greetings from Invoice listings! </h1>"
				+ "<h2><a href='http://localhost:8080/'>Home</a> &bull; &nbsp;"
				+ "<a href='http://localhost:8080/invoices'>All invoices</a> &bull; &nbsp;"
				+ "<a href='http://localhost:8080/inv/add'>Add invoice</a> &bull; &nbsp;"
				+ "<a href='http://localhost:8080/h2' target=_blank>H2 database </a><span style='font-size:0.6em;'>(JDBC URL: <i>"+h2datasource+"</i>, User:<i>"+h2datasourceUsername+"</i>, Pwd:<i>empty</i>)</span></h2>"
				+ "<hr>"+message+"<hr>"
				;

		
	}
	
	String displayFormattedDate(Date date) {
		if (date == null)
			return "";
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String stringDate= simpleDateFormat.format(date);
		return stringDate;
	}
	
	public static boolean isNumeric(String strNum) {
	    if (strNum == null) {
	        return false;
	    }
	    try {
	        double d = Double.parseDouble(strNum);
	    } catch (NumberFormatException nfe) {
	        return false;
	    }
	    return true;
	}
	
	public String showError(String errorMessage) {
		return menuTop(errorMessage);
	}
}
