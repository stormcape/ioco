package com.iocotask.invoices.entities;

import java.util.Date;

public class InvoiceDto {
	public String client;
	public long vatRate;
	public Date invoiceDate;
	
	public InvoiceDto(String client, long vatRate, Date invoiceDate) {
		super();
		this.client = client;
		this.vatRate = vatRate;
		this.invoiceDate = invoiceDate;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public long getVatRate() {
		return vatRate;
	}

	public void setVatRate(long vatRate) {
		this.vatRate = vatRate;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	
	
	
}
