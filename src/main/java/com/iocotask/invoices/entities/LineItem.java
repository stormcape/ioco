package com.iocotask.invoices.entities;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class LineItem {

	@Id @GeneratedValue
	public Long id;
	public long quantity;	
	public String description;
	public BigDecimal unitPrice;
	public long invoiceId;
	
	public LineItem () {}
	
	
	
	public LineItem(long quantity, String description, BigDecimal unitPrice, long invoiceId) {
		
		this.quantity = quantity;
		this.description = description;
		this.unitPrice = unitPrice;
		this.invoiceId = invoiceId;
	}



	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public long getQuantity() {
		return quantity;
	}
	
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getUnitPrice() {
		return unitPrice;
	}
	
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	
}
