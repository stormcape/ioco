package com.iocotask.invoices.entities;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;


@Data
@Entity
public class Invoice {
	public @Id @GeneratedValue Long id;
	public String client;
	public long vatRate;
	public Date invoiceDate;

    @OneToMany(mappedBy="invoiceId")
	private Set<LineItem> items;

	
//	@Autowired
//	private final LineItemRepository lineItemRespository;
	
	
//	Invoice (LineItemRepository lineItemRespository) {
//		this.lineItemRespository = lineItemRespository;
//	}

	public Invoice () {}
	
	
	public Invoice(String client, long vatRate, Date invoiceDate) {
		super();
		this.client = client;
		this.vatRate = vatRate;
		this.invoiceDate =  invoiceDate;
	}

	public Invoice(InvoiceDto invoiceDto) {
		
		this.client = invoiceDto.client;
		this.vatRate = invoiceDto.vatRate;
		this.invoiceDate =  invoiceDto.invoiceDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public long getVatRate() {
		return vatRate;
	}

	public void setVatRate(long vatRate) {
		this.vatRate = vatRate;
	}


	public Date getInvoiceDate() {
		return invoiceDate;
	}


	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	
	  
	  
	public BigDecimal getTotal() {
		Set<LineItem> lines = this.items;

		BigDecimal total = BigDecimal.ZERO;
		if (lines != null && lines.size() > 0) {
			 for (LineItem temp : lines) {
				 total = total.add( temp.unitPrice.multiply(new BigDecimal(temp.quantity)) );
		     }
		}
		return total;
	}
	
}
