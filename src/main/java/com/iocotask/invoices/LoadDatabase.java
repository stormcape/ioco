package com.iocotask.invoices;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.iocotask.invoices.entities.Invoice;
import com.iocotask.invoices.entities.LineItem;
import com.iocotask.invoices.repositories.InvoiceRepository;
import com.iocotask.invoices.repositories.LineItemRepository;

import lombok.extern.slf4j.Slf4j;


@Configuration
@Slf4j
class LoadDatabase {
	Logger logger = LoggerFactory.getLogger(LoadDatabase.class);
	
  @Bean
  CommandLineRunner initDatabase(InvoiceRepository invoiceRepository,
		  LineItemRepository lineItemRepository) {
	  
	  logger.info("Preloading ");
	  long invoiceCount = invoiceRepository.count();
	  if (invoiceCount > 0 ) {
		  return args -> { logger.info("Preloaded before");};
	  }
	
    return args -> {
    	
    	logger.info("Preloading");
    	
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    	
		
		Date invoiceDate = null;
		try {
			invoiceDate = formatter.parse("2020-01-21");
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	Invoice invoice = new Invoice("Anderson",15,invoiceDate);
    	invoiceRepository.save(invoice);
    	
    	long inv_1_id= invoice.id;

		try {
			invoiceDate = formatter.parse("2020-02-20");
		} catch (ParseException e) {
			e.printStackTrace();
		}

    	invoice = new Invoice("Bidvest",15,invoiceDate);
    	invoiceRepository.save(invoice);
    	
    	//long inv_2_id= invoice.id;

    	LineItem inv1_line_1 = new LineItem(5L,"axles",new BigDecimal(100),inv_1_id);
    	lineItemRepository.save(inv1_line_1);
    	
    	LineItem inv1_line_2 = new LineItem(5L,"arbors",new BigDecimal(58),inv_1_id);
    	lineItemRepository.save(inv1_line_2);
    	
    };

  }
}