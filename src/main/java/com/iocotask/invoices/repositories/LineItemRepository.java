package com.iocotask.invoices.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iocotask.invoices.entities.LineItem;


public interface LineItemRepository extends JpaRepository<LineItem, Long>{
	List<LineItem> findByInvoiceId(long invoiceId);
}
